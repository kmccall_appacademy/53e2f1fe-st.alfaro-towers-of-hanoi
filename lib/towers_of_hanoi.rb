require 'byebug'
# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def play
    until won?
      proc_action(input)
    end
    puts "Congratulations, you won!"
  end

  def input
    puts "Action :"
    op = gets.chomp
    op
  end

  def proc_action(action)
    if action == "render"
      render
    elsif action == "move"
      puts "From tower: "
      from_tower = gets.chomp
      puts "To tower: "
      to_tower = gets.chomp
      move(from_tower.to_i, to_tower.to_i)
      render
    else
      "Invalid action, render or move allowed"
    end
  end

  def render
    (@n_disks - 1).downto(0).each do |idx|
      line_output = String.new
      towers.each do |tower|
        line_output << "\t|" unless tower[idx]
        line_output << "\t#{tower[idx]}" if tower[idx]
      end
      puts line_output
    end
    header = String.new
    separator = "\t"

    towers.length.times do |t|
      separator << "------"
      header << "\t|T #{t}"
    end
    puts separator
    puts header
  end

  def initialize
    @n_disks = 3
    @towers = [3.downto(1).to_a, [], []]
  end

  def won?
    towers.each_with_index do |tower, idx|
      next if idx == 0
      return true if in_order?(tower)
    end
    false
  end

  def in_order?(tower)
    return false if tower.empty? || tower.length != @n_disks
    tower.each_with_index do |value, idx|
      next if idx == tower.length - 1
      return false unless value - tower[idx + 1] == 1
    end
    true
  end

  def move(from_tower, to_tower)
    towers[to_tower] << towers[from_tower].pop if valid_move?(from_tower, to_tower)
  end

  def valid_move?(from_tower, to_tower)
    return true if towers[to_tower].empty?
    return false if towers[from_tower].empty?
    towers[from_tower].last < towers[to_tower].last
  end


end
